<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'WordpressEval' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'whitediablo' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';,CG-i!3k(Y<dtfUcK!)0umL-U^#q/d]Hsvv2HUo~y!dk_Oi#FS|I+2Kgw.b#.>(' );
define( 'SECURE_AUTH_KEY',  'kY0mfgU&l$]Y> ZV(ydYoE4N*k>]-yd<I,hI2=W~I{q!,j0nL}iPm^w`MF@okP>;' );
define( 'LOGGED_IN_KEY',    '9`ym1&}WCqx4L}@_I!!|fi86Dj oUdMqD48,-@]%SG{,wZ+.kR2a!|I-GzY/t]K{' );
define( 'NONCE_KEY',        '_#y$3~!c@o5Q`0%he._%i?`5HB?adJeM=Dgv77:!U&5kb[KW.4}!R38WE6EgqO=b' );
define( 'AUTH_SALT',        'J?S^4SI~l{#a#Rk{H&M$[rT=<fsUwj{NA:Dg8lftT6YJ})aC]efzfqj8`?<I;3OU' );
define( 'SECURE_AUTH_SALT', '#km6vPVgheqlE HxyzFcVhA}}~l y!+mr|{,v&=)D0%4G%~{_c.eC_1oK3oRVY{.' );
define( 'LOGGED_IN_SALT',   'fzG9_b(h|lx51wFr9QMwI.,{=85vOK`>E|z4)jaBAO^9I,|NH|9.xY9g=8oiYDuf' );
define( 'NONCE_SALT',       'xTVbmM#J8)!u4{Z,y}f@S`w^q-]0D)iwx%K1rS2`a^w,i2/m;ALJ]y.bKPsX]XWc' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
